﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_most
{
    class SharedNote : Note
    {
        private List<string> users;
        public SharedNote(string message, ITheme theme, string owner) : base(message, theme)
        {
            users = new List<string>();
            users.Add(owner);
        }

        public void AddUser(string user)
        {
            users.Add(user);
        }

        public void RemoveUser(string user)
        {
            users.Remove(user);
        }



        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("REMINDER: ");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            Console.WriteLine("SHARED WITH");
            foreach (String user in users)
            {
                Console.WriteLine(user + " ");
            }
            Console.ResetColor();
        }
    }
}
