﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_most
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme1 = new LightTheme();
            Note note1 = new ReminderNote("Prijavi ispite!!", theme1);

            ITheme theme2 = new RedTheme();
            SharedNote note2 = new SharedNote("Glory Glory Man United", theme2, "Dario");

            note2.AddUser("Mario");
            note2.AddUser("Kerum");

            note1.Show();
            note2.Show();

            /*
            Notebook notebook = new Notebook();
            notebook.AddNote(note1);
            notebook.ChangeTheme(theme2);
            notebook.AddNote(note2);
            notebook.Display();
            */
            

            Notebook notebook = new Notebook(theme1);
            notebook.AddNote(note2);
            notebook.Display();

            Notebook notebook2 = new Notebook(theme2);
            notebook2.AddNote(note1);
            notebook2.Display();
        }
    }
}
