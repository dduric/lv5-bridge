﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_most
{
    class RedTheme : ITheme
    {
        public string GetFooter(int width)
        {
            return new string('/', width);
        }

        public string GetHeader(int width)
        {
            return new string('*', width);
        }

        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.DarkGray;
        }

        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
        }
    }
}
